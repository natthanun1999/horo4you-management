import Vue from 'vue'

Vue.filter('showPriceFormat', (number = 0) => {
  const num = parseFloat(number)
  if (num === 0 || num) {
    return parseFloat(num).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  }
  return '-'
})

Vue.filter('showNumberFormat', (number = 0) => {
  const num = parseFloat(number)
  if (num === 0 || num) {
    return parseFloat(number).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  }

  return '-'
})

Vue.filter('toCapitalize', (text) => {
  if (!text) {
    return '-'
  }

  const result = text.replace('_', ' ')

  return result.replace(/(?:^|\s|[-"'([{])+\S/g, (c) => c.toUpperCase())
})
