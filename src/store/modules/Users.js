const state = {
  user: null,
  types: [
    'renter',
    'rerenter',
    'owner',
    'employee',
    'other'
  ]
}

const actions = {
  setUser ({ commit }, payload) {
    commit('SET_USER', payload)
  },
  setTypes ({ commit }, payload) {
    commit('SET_TYPES', payload)
  }
}

const mutations = {
  SET_USER (state, payload) {
    state.user = payload
  },
  SET_TYPES (state, payload) {
    state.types = payload
  }
}

const getters = {
  user: (state) => state.user,
  types: (state) => state.types
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
