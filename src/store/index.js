import Vue from 'vue'
import Vuex from 'vuex'
import UsersModule from './modules/Users'
import SnackbarModule from './modules/Snackbar'
import LoadingModule from './modules/Loading'
import ConfirmDialogModule from './modules/ConfirmDialog'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    Users: UsersModule,
    Snackbar: SnackbarModule,
    Loading: LoadingModule,
    ConfirmDialog: ConfirmDialogModule
  }
})
