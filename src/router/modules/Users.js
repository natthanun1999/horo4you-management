import auth from '../../middlewares/auth'

export default {
  path: '/users',
  component: () => import('@/views/users/index.vue'),
  children: [
    {
      path: '',
      name: 'Users',
      component: () => import('@/views/users/pages/Users.vue'),
      meta: {
        middleware: auth
      }
    }
  ]
}
