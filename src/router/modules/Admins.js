import auth from '../../middlewares/auth'

export default {
  path: '/admins',
  component: () => import('@/views/admins/index.vue'),
  children: [
    {
      path: '',
      name: 'Admins',
      component: () => import('@/views/admins/pages/Admins.vue'),
      meta: {
        middleware: auth
      }
    }
  ]
}
