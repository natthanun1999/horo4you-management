import auth from '../../middlewares/auth'

export default {
  path: '/answers',
  component: () => import('@/views/answers/index.vue'),
  children: [
    {
      path: '',
      name: 'Answers',
      component: () => import('@/views/answers/pages/AnswersList.vue'),
      meta: {
        middleware: auth
      }
    }
  ]
}
