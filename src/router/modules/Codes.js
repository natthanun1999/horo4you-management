import auth from '../../middlewares/auth'

export default {
  path: '/codes',
  component: () => import('@/views/codes/index.vue'),
  children: [
    {
      path: '',
      name: 'Codes',
      component: () => import('@/views/codes/pages/CodesList.vue'),
      meta: {
        middleware: auth
      }
    }
  ]
}
