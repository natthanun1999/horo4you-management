import auth from '../../middlewares/auth'

export default {
  path: '/answer-logs',
  component: () => import('@/views/answer-logs/index.vue'),
  children: [
    {
      path: '',
      name: 'AnswerLogs',
      component: () => import('@/views/answer-logs/pages/AnswerLogsList.vue'),
      meta: {
        middleware: auth
      }
    }
  ]
}
