import Vue from 'vue'
import VueRouter from 'vue-router'
import { getAuth } from '../utils/auth.utils'
import Login from '../views/Login.vue'
import Users from './modules/Users'
import Admins from './modules/Admins'
import Codes from './modules/Codes'
import Answers from './modules/Answers'
import AnswerLogs from './modules/AnswerLogs'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/logout',
    name: 'Logout',
    component: Login
  },
  Codes,
  Users,
  Admins,
  Answers,
  AnswerLogs
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.name !== 'Login' && !getAuth()) {
    return next({
      name: 'Login'
    })
  }
  return next()
})

export default router
