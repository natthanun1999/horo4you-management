import axios from 'axios'
import { getAuthToken } from '../utils/auth.utils'

class UploaderProvider {
  // eslint-disable-next-line class-methods-use-this
  async uploadFile (file, zodiac, birth, luck) {
    const authData = getAuthToken()
    const formData = new FormData()

    formData.append('file', file)
    formData.append('zodiac', zodiac)
    formData.append('birth', birth)
    formData.append('luck', luck)

    const { data } = await axios.post(`${process.env.VUE_APP_API}/upload`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        [authData.key]: authData.value
      }
    })

    return data
  }
}

export default UploaderProvider
