import HttpRequest from './http-request'
import { getAuthToken } from '../utils/auth.utils'

class AdminsProvider extends HttpRequest {
  getAllAdmins (query) {
    this.setHeader(getAuthToken())
    return this.get('/admins', query)
  }

  getAdminById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/admins/${id}`)
  }

  getAdminByUsername (username) {
    this.setHeader(getAuthToken())
    return this.get(`/admins/${username}/username`)
  }

  create (payload) {
    this.setHeader(getAuthToken())
    return this.post('/admins', payload)
  }

  updateAdminById (payload) {
    this.setHeader(getAuthToken())
    return this.patch(`/admins/${payload.id}`, payload)
  }

  deleteAdminById (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/admins/${id}`)
  }
}

export default AdminsProvider
