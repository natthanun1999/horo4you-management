import HttpRequest from './http-request'
import { getAuthToken } from '../utils/auth.utils'

class UsersProvider extends HttpRequest {
  getAllUsers (query) {
    this.setHeader(getAuthToken())
    return this.get('/users', query)
  }

  getUserById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/users/${id}`)
  }

  clearFallbackById (id) {
    this.setHeader(getAuthToken())
    return this.patch(`/users/${id}/clear-fallback`)
  }

  clearCodeById (id) {
    this.setHeader(getAuthToken())
    return this.patch(`/users/${id}/clear-code`)
  }

  deleteUserById (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/users/${id}`)
  }
}

export default UsersProvider
