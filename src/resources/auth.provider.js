import HttpRequest from './http-request'

class AuthProvider extends HttpRequest {
  login (formData) {
    return this.post('/public/admins/login', formData)
  }
}

export default AuthProvider
