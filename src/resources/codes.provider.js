import axios from 'axios'
import HttpRequest from './http-request'
import { getAuthToken } from '../utils/auth.utils'

class CodesProvider extends HttpRequest {
  getAllCodes (query) {
    this.setHeader(getAuthToken())
    return this.get('/codes', query)
  }

  getCodeById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/codes/${id}`)
  }

  createCode (payload) {
    this.setHeader(getAuthToken())
    return this.post('/codes', payload)
  }

  // eslint-disable-next-line class-methods-use-this
  async importCodes (file) {
    const authData = getAuthToken()
    const formData = new FormData()

    formData.append('file', file)

    const { data } = await axios.post(`${process.env.VUE_APP_API}/codes/import`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        [authData.key]: authData.value
      }
    })

    return data
  }

  updateCodeById (payload) {
    this.setHeader(getAuthToken())
    return this.put(`/codes/${payload.id}`, payload)
  }

  deleteCodeById (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/codes/${id}`)
  }
}

export default CodesProvider
