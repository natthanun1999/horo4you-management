import HttpRequest from './http-request'
import { getAuthToken } from '../utils/auth.utils'

class AnswersProvider extends HttpRequest {
  getAllAnswers (query) {
    this.setHeader(getAuthToken())
    return this.get('/answers', query)
  }

  getAnswerById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/answers/${id}`)
  }

  createAnswer (payload) {
    this.setHeader(getAuthToken())
    return this.post('/answers', payload)
  }

  updateAnswerById (payload) {
    this.setHeader(getAuthToken())
    return this.put(`/answers/${payload.id}`, payload)
  }

  deleteAnswerById (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/answers/${id}`)
  }
}

export default AnswersProvider
