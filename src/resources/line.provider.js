import HttpRequest from './http-request'
import { getAuthToken } from '../utils/auth.utils'

class LineProvider extends HttpRequest {
  sendMessages (payload) {
    this.setHeader(getAuthToken())
    return this.post('/line/push-message', payload)
  }
}

export default LineProvider
