import HttpRequest from './http-request'
import { getAuthToken } from '../utils/auth.utils'

class AnswerLogsProvider extends HttpRequest {
  getAllAnswerLogs (query) {
    this.setHeader(getAuthToken())
    return this.get('/answer-logs', query)
  }

  getAnswerLogById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/answer-logs/${id}`)
  }

  deleteAnswerLogById (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/answer-logs/${id}`)
  }
}

export default AnswerLogsProvider
